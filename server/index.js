require('babel-core/register')
require('babel-polyfill')
import express from 'express'
import morgan from 'morgan'
import bodyParser from 'body-parser'
import methodOverride from 'method-override'
import mongoose from 'mongoose'
import passport from 'passport'
import SteamStrategy from 'passport-steam'
import routes from './routes/index.js'
import path from 'path'
import config  from'config'

const app = express()
//const port = config.port
  let port= process.env.PORT || 8080;
let configDb = config.get('Valley.dbConfig.host.simracing')
try{
  mongoose.connect(configDb, { useNewUrlParser: true })
  mongoose.set('useCreateIndex', true)
}
catch(error){
  console.log(error)
}
app.use(morgan('dev'))
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({extended: false}))
app.use(passport.initialize())


app.use('/api', routes)
app.use('/static/others/', express.static('public'));
app.use( express.static(path.join(__dirname, '../client/build')));
app.get('/*', function (req, res) {
    res.sendFile(path.join(__dirname, '../client/build', 'index.html'));
});
app.listen(port , () => { console.log('Runing on port: ' + port) })
