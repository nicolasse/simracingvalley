import mongoose from 'mongoose'
import config from 'config'

const dbUser = config.get('Valley.dbConfig.host.users')
let connection = mongoose.createConnection(dbUser, { useNewUrlParser: true })

mongoose.Promise = global.Promise

const UserSchema = new mongoose.Schema({
  _id: {
    type: String,
    unique: true,
  },
  steam_id: {
    type: String,
  },
  username: {
    type: String,
  },
  password: {
    type: String,
  },
  avatar: {
    type: String,
  },
  admin: {
    type: Boolean,
    default: false,
  },
  email: {
    type: String,
  },
  email_confirmation_sent_on: {
    type: Number,
  },
  email_confirmed: {
    type: Boolean,
    default: false,
  },
  email_confirmed_on: {
    type: Number,
  },
  lastname: {
    type: String,
  },
  lastpassdate: {
    type: Number,
  },
  name: {
    type: String,
  },
  about: {
    type: String,
  },
  birthday: {
    type: String,
  },
  city: {
    type: String,
  },
  gender: {
    type: String,
  },
  phrase: {
    type: String,
  },
  state: {
    type: String,
  },
  email_token: String,
},
)

module.exports = connection.model('User', UserSchema)
