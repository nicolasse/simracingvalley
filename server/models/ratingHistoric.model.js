import mongoose from 'mongoose'

mongoose.Promise = global.Promise

const RatingSchema = mongoose.Schema({
  filename: {
    type: String,
  },
  result: [
    {
      steamID: String,
      mu: Number,
      sigma: Number,
      points: Number,
    }
  ]
},
{
  collection: 'HistRating'
}
)

module.exports = mongoose.model('HistRating', RatingSchema)
