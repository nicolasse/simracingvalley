import mongoose from 'mongoose'

mongoose.Promise = global.Promise

const IncidentSchema = mongoose.Schema({
  filename: {
    type: String,
  },
  result: [
    {
      steamID: String,
      incidents: Number,
      raceincidents: Number,
    }
  ]

},
{
  collection: 'HistIncident'
})

module.exports = mongoose.model('HistIncident', IncidentSchema)
