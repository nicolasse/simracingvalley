import mongoose from 'mongoose'

mongoose.Promise = global.Promise

const VoteSchema = mongoose.Schema({
  resultid: {
    type: mongoose.Schema.Types.ObjectId,
  },
  upvote: [{
    voterid: String,
    userid: String,
    username: String,
  }],
  downvote: [{
    voterid: String,
    userid: String,
    username: String,
  }]
}, 
  {collection: 'ResultVote'})

module.exports = mongoose.model('ResultVote', VoteSchema)
