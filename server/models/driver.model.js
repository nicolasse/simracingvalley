import mongoose from 'mongoose'
import config from 'config'
const dbConfig = config.get('Valley.dbConfig')
let connection = mongoose.createConnection(dbConfig.host.simracing, { useNewUrlParser: true } )

mongoose.Promise = global.Promise;

const DriverSchema = new mongoose.Schema({
  unique_name: {
    type: String,
  },
  races_done: {
    type: Number,
    default: 0,
  },
  steamID: {
    type: String,
    required: true,
  },
  rating: {
    mu: {
      type: Number,
      default: 0,
    },
    sigma: {
      type: Number,
      default: 0,
    },
  },
  points: {
    type: Number,
  },
  top10: {
    1: {
      type: Number,
      default: 0,
    },
    2: {
      type: Number,
      default: 0,
    },
    3: {
      type: Number,
      default: 0,
    },
    4: {
      type: Number,
      default: 0,
    },
    5: {
      type: Number,
      default: 0,
    },
    6: {
      type: Number,
      default: 0,
    },
    7: {
      type: Number,
      default: 0,
    },
    8: {
      type: Number,
      default: 0,
    },
    9: {
      type: Number,
      default: 0,
    },
    10: {
      type: Number,
      default: 0,
    },
  },
  penalized: {
    type: Boolean,
  },
  incidents: {
    type: Number,
    default: 0,
  },
  classimg: {
    type: String,
    default: 'nao_ranqueado.png',
  },
  pole: {
    type: Number,
    default: 0,
  },
  rank_pos: {
    type: Number,
  },
  incident_ave: {
    type: Number
  },
  achievements: [
    {
      achi_name: String,
      fa_image: String,
      fa_style: String,
    }
  ],
  votes:{
    type: Number,
    default: 0,
  }
},{ collection: 'Drivers' })


module.exports = connection.model('Driver', DriverSchema)
