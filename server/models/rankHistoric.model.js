import mongoose from 'mongoose'

mongoose.Promise = global.Promise

const RankHistSchema = mongoose.Schema({
  filename: {
    type: String,
  },
  positions: [
    {
      points: Number,
      steamID: String,
      rank_pos: Number,
    }
  ]
},
  {
    collection: 'HistRank'
  }

)

module.exports = mongoose.model('HistRank', RankHistSchema)
