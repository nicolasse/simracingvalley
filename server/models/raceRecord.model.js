import mongoose from 'mongoose'

mongoose.Promise = global.Promise

const RaceRecordSchema = mongoose.Schema({
  car: [
    {type: String},
  ],
  track: [
    {type: String},
  ],
  laprecord: [
    {
      racedate : String,
      s1 : String,
      s2 : String,
      s3 : String,
      laptime : String,
      steamID : String,
      userid : String,
      username : String,
    }
  ],
  resultid: {
    type: mongoose.Schema.ObjectId,
    ref: 'RaceResult' 
  } 
}, {collection: 'RaceRecord'})

module.exports = mongoose.model('RaceResult', RaceRecordSchema)
