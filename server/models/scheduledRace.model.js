import mongoose from 'mongoose'

mongoose.Promise = global.Promise

const ScheduledSchema = mongoose.Schema({
  tracks: [
    {type: String,}
  ],
  car:[ 
    {
    type: String,
    }
  ],
  date: {
    type: String,
  },
  time: {
    type: String,
  },
  carviews: [
    {type: String}
  ],
  damages:[
  {
    type: String,
  },
  ],

  fixsetups: [
    {
      type: String,
    }
  ],
  fueltires:[
  {
    type: String,
  }
  ],
  pitreturns: [
    { type: String }
  ],
  help: [
  {
    type: String,
  }
  ],
  mechfailures: [{
    type: String,
  }
  ],

  maxplayers: {
    type: String,
  },
  ip: {
    type: String,
  },
  password: {
    type: String,
  },
  flags: [
    { type: String }
  ],
  tiresets:[{
    type: String,
  }
  ],
  session:[
    {
      type: String,
    },
  ],
  starttime: [{
    type: String,
  }],
  starttypes: [{
    type: String,
  }],
  trackconds: [{
    type: String,
  }],
  trackprogresses: [{
    type: String,
  }],
  Started: {
    type: Boolean,
  },
  racefinishes: [
    { type: String }
  ],
  downstream: {
    type: String,
  },
  upstream: {
    type: String,
  },
  fixupgrades: [
    {type: String,}
  ],
  warmups: [
    {type: String,}
  ],
  privatequalies: [
    {type: String,}
  ],
  timescales: [
    {type: String,}
  ],

  participants: [{
    username: String,
    userid: String,
    steamid: String,
  }],
  Done: {
    type: Boolean,
  },
  user: {
    id : String,
    username: String,
  },
  official: {
    type: Boolean,
  },
  cdc: {
    type: Boolean,
  },
  timestamp: {
    type: Number
  },


},
  {collection: 'ScheduledRace'})

module.exports = mongoose.model('ScheduledRace', ScheduledSchema)
