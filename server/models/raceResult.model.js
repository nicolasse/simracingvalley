import mongoose from 'mongoose'

mongoose.Promise = global.Promise

const RaceSchema = mongoose.Schema({
  timestamp: {
    type: String,
  },
  racedate: {
    type: String,
  },
  practicefilename:{
    type: String,
  },
  practice: [
    {
    position: String,
    driver: String,
    laps: [{
      position: String,
      fuel: String,
      s1: String,
      s1: String,
      s3: String,
      laptime: String,
      bests1: Boolean,
      bests1: Boolean,
      bests3: Boolean,
      bestlap: Boolean,
    }],
    steamID: {
      type: String,
    },
    userid: {
      type: String,
    },
    incidents: {
      type: Number,
    },
    finishstatus: {
      type: String,
    },
    bestlap: {
      type: String,
    }
    }
  ],
  qualifyfilename:{
    type: String,
  },
  qualify:[
    {
      position: String,
      driver: String,
      laps:[{
        position: String,
        fuel: String,
        s1: String,
        s2: String,
        s3: String,
        laptime: String,
        bests1: Boolean,
        bests2: Boolean,
        bests3: Boolean,
        bestlap: Boolean,
      }],
    steamID: {
      type: String,
    },
    userid: {
      type: String,
    },
    incidents: {
      type: Number,
    },
    finishstatus: {
      type: String,
    },
    bestlap: {
      type: String,
    }
    }
  ],
  racefilename:{
    type: String,
  },
  race: [{
    position: String,
    driver: String,
    laps:[{
      position: String,
      fuel: String,
      s1: String,
      s2: String,
      s3: String,
      laptime: String,
      bests1: Boolean,
      bests2: Boolean,
      bests3: Boolean,
      bestlap: Boolean,
    }],
    fulltime:{
      type: String,
    },
    steamID: {
      type: String,
    },
    userid: {
      type: String,
    },
    incidents:{
      type: Number,
    },
    st_position:{
      type: String,
    },
    lapsled:{
      type: String,
    },
    finishstatus:{
      type: String,
    },
    bestlap:{
      type: String,
    }
  }],
  rated: Boolean,
  srvsettings: {
    id: String,
    tracks: [{type: String}],
    cars: [{type: String}],
    date: String,
    time: String,
    participants: [{
      username: String,
      userid: String,
      steamid: String,
      rankpos: Number,
      classimg: String,
      points: Number,
      incidents: Number,
      sponsor: Boolean,
      
    }],
    Done: Boolean,
    carview: String,
    damage: String,
    fixsetup: String,
    fueltire: String,
    helps: String,
    ip: String,
    maxplayer: String,
    mechfailure: String,
    password: String,
    rules: String,
    session:[{
      type: String,
    }],
    starttime: String,
    starttype: String,
    tirerests: String,
    trackcond: String,
    trackprogress: String,
    maxplayer: String,
    Started: Boolean,
    user: {
      id: String,
      username: String,
    },
    official: Boolean,
    cdc: Boolean,
    public: Boolean,
    Online: Boolean,
    Close: Boolean,
    timestamp_start: Number,
    timestamp_end: Number,

  }
},
  {collection: 'RaceResult'})
module.exports = mongoose.model('RaceSchema', RaceSchema)

