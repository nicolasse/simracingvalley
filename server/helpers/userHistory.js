import HistIncident from '../models/incidentHistoric.model'
import HistRating from '../models/ratingHistoric.model'
import HistRank from '../models/rankHistoric.model'

exports.incidents =  (steamID) => {
  let detailPromise = new Promise((resolve) => {
    HistIncident.find({'result.steamID': steamID})
    .select({ 'filename': 1, 'result': { $elemMatch: { steamID: steamID } } })
    .lean()
    .then((result)=> {
        let incidentsLabels = getIncidents( result )
        let historic =  {labels: jsonIterator(result, 'filename'),
        datasets: [
          {
            label: 'Medida Incidentes a cada 5 corridas',
            data: incidentsLabels.avg5
          },
          {
            label: 'Incidente por Corrida',
            data: incidentsLabels.race
          }
        ]
      }
      resolve(historic)
    }
    )
    .catch(err => { err })
    })
  return detailPromise
}

exports.rating = (steamID) => {
  let detailPromise = new Promise(resolve => {
   HistRating.find({'result.steamID': steamID}) 
    .select({'filename': 1, 'result': { $elemMatch: { steamID: steamID } }})
    .lean()
      .then(result => {
        let historic = { points: [0], rangeError: [0] }
        for( let i = 0; i < result.length; i++ ){
          historic.rangeError.push( (result[i].result[0].mu * 1000 ).toFixed(2))
          historic.points.push( (result[i].result[0].points * 1000 ).toFixed(2))
        }
        let dataFormat = {
          labels: jsonIterator(result, 'filename'),
          datasets: [
            {
              label: 'Histórico de Pontos',
              data: historic.points
            },
            {
              label: 'Range de Error',
              data: historic.rangeError
            }
          ]
          }
        resolve(dataFormat) 
      })
      .catch(err => {err})
  })
    return detailPromise
}

exports.ranking = (steamID) => {
  let detailPromise = new Promise(resolve => {
   HistRank.find({ 'positions.steamID': steamID }) 
    .select({'filename': 1, 'positions':{ $elemMatch: { steamID: steamID } } })
    .lean()
      .then(result => {
        //let rankHist = result.positions.find( histo => histo.steamID === steamID )
        let historic = result.map( e => { return e.positions[0].rank_pos})
        historic.unshift(0)
        let dataFormat = {
          labels: jsonIterator(result, 'filename'),
          datasets: [
            {
              label: 'Posição no Rank',
              data: historic
            },
          ]
          }
        resolve(dataFormat) 
      })
      .catch(err => {err})
  })
    return detailPromise
}

const getIncidents = (obj) => {
  let arrAvg = [0]
  let avg5 = [0]
  let perRace = [0]

  for(let i = 0; i < obj.length; i++){
    if(avg5.length === 5) {
      avg5.shift()
    }
    let raceIncident = obj[i].result[0].raceincidents
    avg5.push( raceIncident )
    perRace.push( raceIncident )
    arrAvg.push( (avg5.reduce( (a, b) => {return  a+b } ) / avg5.length ).toFixed(2))
  }
  return {avg5: arrAvg, race: perRace}
}

const jsonIterator = (obj, field) => {
  let arr = ['Start']
  for(let key in obj){
    let filenameSplited = obj[key][field].split('_')
    let date = filenameSplited[2] + '/'+filenameSplited[1] + '/' + filenameSplited[0].substring(2,4)
    arr.push(date)
  }
  return arr
}

