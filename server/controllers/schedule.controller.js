import ScheduledRace from '../models/scheduledRace.model'
import User from '../models/user.model'
import jwt from 'jsonwebtoken'
import config from 'config'

const secretWord = config.get('Valley.secretWord')

exports.list = ( req, res ) => {
  try{
    let token = req.headers['x-access-token'] 
    let decoded = jwt.verify( token, secretWord )
    User.findOne({ _id: decoded.userId })
      .then( user => {
        if(user){
  ScheduledRace.aggregate()
      .group(
        {
          _id: '$date',
          events: { 
            $push: {
              cars: '$cars',
              tracks: '$tracks',
              time: '$time',
              cdc: '$cdc',
              password: '$password',
              official: '$official',
              online: '$Online',
              user: '$user',
              participants: '$participants',
              close: '$Close',
              public: '$public',
              timestamp_start: '$timestamp_start',
              timestamp_end: '$timestamp_end',
            } }
        })
      .project({ events: 1})
            .then( response => {
              response = response.map( race => { 
                if(race.cdc){
                  race.password = 'CONVIDADOS'
                }
                return race
              } )
              let sortedByDate = response.sort(( a, b)  => {
                return a._id.split("-").reverse().join() > b._id.split("-").reverse().join()
              })
              res.status(200).json(sortedByDate)
            } )
            .catch( error => {
              res.status(404).json({ error: true, message: error.message })
            } )

              }
        else{
          throw ( new Error('not logged') )
        }
            } )

  }
  catch( err)  
    {

  ScheduledRace.aggregate()
      .group(
        {
          _id: '$date',
          events: { 
            $push: '$$ROOT' }
        })
      .project({ events: 1})
    .then( response => {
      let sortedByDate = response.sort(( a, b)  => {
          return a._id.split("-").reverse().join() > b._id.split("-").reverse().join()
      })
      res.status(200).json(sortedByDate)
    } )
    .catch( error => {
      console.log(error)
      res.status(404).json({ error: true, message: error.message })
    } )
  }
}

