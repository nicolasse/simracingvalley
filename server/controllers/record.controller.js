import RaceRecord from '../models/raceRecord.model'

exports.listTracks = (req, res) => {
  RaceRecord
    .distinct('track.0')
    .then(( tracks ) => {
      res.status(200).json( tracks.sort() )
    })
    .catch(err =>{
      console.log(err)
      res.status(500).json({error: true, message: 'Error Fetching tracks'})
    })
}

exports.listCars = (req, res) => {
  let decodedURI = decodeURIComponent(req.params.track)

  RaceRecord.find({'track.0': decodedURI})
    .select('car')
    .lean()
    .then( response => {
      let carsList = response.map( carObject => {  return carObject.car[0]})
      res.status(200).json(carsList.sort())
    })
    .catch(err => {
      console.log(err)
      res.status(500).json({error: true, messge: 'Error Fetching cars'})
    })
}

exports.listRecords = (req, res) => {
  let decodedTrack = decodeURIComponent(req.params.track)
  let decodedCar = decodeURIComponent(req.params.car)
  RaceRecord.find({'track.0': decodedTrack, 'car.0': decodedCar})
    .select('laprecord')
    .lean()
    .then( response => {
      let records = response[0].laprecord
      res.status(200).json(records)
    } )
    .catch(err => {
      console.log(err)
      res.status(500).json({error: true, message: 'Error fetching records'})
    })
}
