import mongoose from 'mongoose'
import VoteResult from '../models/voteResult.model'
import RaceResult from '../models/raceResult.model'
import User from '../models/user.model'

exports.vote = ( req, res ) => {
  let race = req.params.id
  let driver = req.params.driver
  let usernameLogged = req.username
  let userIdLogged = req.userId
  RaceResult.findById(race)
    .then( (raceResult) => {
      let hasParticipated = raceResult.race.find(( driverRace => {
        return driverRace.userid === userIdLogged
      } ))
      if(hasParticipated) return Promise.resolve(hasParticipated)
      return Promise.reject(new Error('Não participou da corrida'))
  })
    .then( hasParticipated  => {
      if(userIdLogged !== driver) {
        return VoteResult.findOne({resultid: mongoose.Types.ObjectId(race) })
      }
      return Promise.reject(new Error('Não pode votar em si mesmo'))
    })
    .then( voteResult => {
        let filteredVotes = voteResult
        if( voteResult ){
          filteredVotes.upvote = voteResult.upvote.filter( 
              vote => vote.voterId !== userIdLogged
            )
            filteredVotes.upvote.push( 
              {
              voterid: userIdLogged,
              userid: driver,
              username: usernameLogged,
              }
            )
          }
          else{
            filteredVotes = new VoteResult({
              voterid: userIdLogged,
              userid: driver,
              username: usernameLogged,
              })
          }
          return filteredVotes.save()
          })
        .then( () => {
          res.status(201).end()
          } )
        .catch( error => {
          res.status(500).json({error: true, message: error.message})
        } )
    }


