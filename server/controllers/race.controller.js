import mongoose from 'mongoose'
import RaceResult from '../models/raceResult.model'
import VoteResult from '../models/voteResult.model'
import Driver from '../models/driver.model'

const errorResponse = { error: true, msg: 'Internal Error' }
exports.list = (req, res) => {
      let totalPages = 0
  RaceResult.estimatedDocumentCount({})
    .then( count => {
      totalPages = Math.ceil(count / 20)
      let page = req.params.page 
      let skip = (page) * 20
      return RaceResult.find({})
      .select({
        'timestamp':1,
        'srvsettings.tracks': 1,
        'srvsettings.cars':1,
        'srvsettings.date': 1,
        'srvsettings.cdc': 1,
        'srvsettings.user': 1,
        'srvsettings.official': 1,
      })
      .sort('-timestamp')
      .skip(skip)
      .limit(20)
      .lean()})
    .then((result) => {
      res.status(200).json({pages: totalPages, races: result})
    })
    .catch(err => {
      console.log(err)
      res.status(500).json( errorResponse )
    })
}

exports.detail = ( req, res ) => {
  let id = req.params.id
   RaceResult.findOne({_id: id})
    .select('-practice.steamID -qualify.steamID  -srvsettings.ip -srvsettings.password  ')
    .lean()
    .then(async(result) => {
    await result.race.map(async( driver) => {
          await Driver.findOne({steamID: driver.steamID})
            .select('points')
            .then( pointsObj => {
               let variation = pointsObj.points * 1000 - (result.srvsettings.participants.find(d => {
                  return d.steamid === driver.steamID})
                ).points
              driver = {...driver, variation: Math.round(variation)}
          } )
            .catch(err => console.log(err))
      } )
      return result
    })
    .then(result => {
      VoteResult.findOne({resultid: id})
        .then( votesResult =>{
            //votes: votesResult
          if(votesResult){
            result = {
              ...result,
              race: result.race.map( driverRace => {
                return {
                  ...driverRace,
                  upvotes: votesResult.upvote.filter( vote => {
                    return vote.userid === driverRace.userid
                  } )
                }
            })}
            
          }
          res.status(200).json(result)
        
        })
    })
    .catch((error) => {
      console.error(error.message)
      res.status(500).json( {error: true, message: error.message} )
    })
}
