import User from '../models/user.model'
import jwt from 'jsonwebtoken'
import config from 'config' 

const secretWord = config.get('Valley.secretWord')

exports.confirmation = (req, res) => {
  let email_token = req.params.token
  let decoded = jwt.verify(email_token, secretWord)
  User.findOne({_id: decoded.id})
    .then(user => {
      user.email_confirmed = true
      user.email_token = null
      return user.save()
    })
    .then(saved => {
      res.status(200).json({error: false, message: 'Email Validated'})
    })
    .catch(err => {
      console.log(err)
      res.status(500).json({error: true, message: 'Error verification token'})
    })


}
