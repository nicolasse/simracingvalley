require('babel-polyfill')
import User from '../models/user.model'
import Driver from '../models/driver.model'
import userHistory from '../helpers/userHistory'

exports.list = (req, res) => {
  let totalPages =  Driver.estimatedDocumentCount({}).then(count => {
    totalPages = Math.ceil(count / 20)
    let page = req.params.page
    let skip = page * 20
    return Driver.find({ rank_pos: {$gt: 0} },
    'rank_pos steamID classimg points incident_ave races_done unique_name top10 votes',
    {
      sort: {rank_pos: 1},
      skip: skip,
      limit: 20,
      lean: true,
    })})
    .then(async drivers => {
      let processed = drivers.map(async (e) => {
      await User.findOne({steam_id: e.steamID}).select({'_id': 1})
          .then(user =>  { e._id = user._id} )
          .catch(err => console.log(err))
        return {
          id: e._id,
          Points: Math.round(e.points * 1000),
          Incidents: (e.incident_ave || 0.00).toFixed(2),
          Name: e.unique_name,
          First: e.top10[1],
          Second: e.top10[2],
          Third: e.top10[3],
          Fourth: e.top10[4],
          Fifth: e.top10[5],
          Races: e.races_done,
          Class: e.classimg,
          Position: e.rank_pos,
          votes: e.votes
        }
      })
      let response =  await Promise.all(processed)
      res.status(200).json({pages: totalPages, driversStats: response})
    })
    .catch( err => {
      console.log(err)
      res.status(500).json({error: true, msg: 'Internal Error'})
    })
}

exports.detail = async (req, res) => {
  let id = ''+req.params.id+''
  let driverStats
  await User.findOne({_id: id})
      .select('-_id steam_id username avatar lastname name about birthday city gender phrase state email_confirmed_on achievements votes sponsor')
      .lean()
      .then((user) =>{
        let email_date = new Date(user.email_confirmed_on * 1000)
        let month = email_date.getMonth() + 1
        let email_confirmed = email_date.getDate()+'-'+month+'-'+email_date.getFullYear()
        driverStats = {
          ...user,
          email_confirmed
        }
      })
      .catch( err => {
        console.log(err)
        res.status(500).json({error: true, msg: 'Internal Error'})
      })
  await  Driver.findOne({steamID: driverStats.steam_id})
          .select({
          'rating': 0,
          'penalized': 0,
          'steamID': 0,
          })
          .lean()
      .then(driver => {
        if(driver){
          return driverStats = {
          ...driverStats,
          ...driver,
          incident_ave: driver.incident_ave ? (driver.incident_ave).toFixed(2) : '0.00',
          points: Math.round( driver.points * 1000),
          }

       }
        else {
          throw new Error('No info yet')
        }
      })
      .catch(err => {
        console.log(err);
        res.status(500).json({error: true, message: err.message, driverStats})
      })

     await userHistory.incidents(driverStats.steam_id).then(result =>{ 
          driverStats = {
            ...driverStats,
           incidentsHistoric: result,
          }
        }
    )
  await userHistory.ranking(driverStats.steam_id).then( result => {
    driverStats = {
      ...driverStats,
      rankingHistoric: result
    }
  } )
  await userHistory.rating(driverStats.steam_id).then(result =>{
    driverStats = {
      ...driverStats,
      ratingHistoric: result,
    }
        return res.status(200).json({driverStats})
  })
      .catch( err => {
        console.log(err)
        res.status(500).json({error: true, msg: 'Internal Error'})
      })
}

exports.search = ( req, res ) => {
  let name = req.params.name
  Driver.find({unique_name: { $regex: name, $options: 'i' } })
    .select('rank_pos steamID classimg points incident_ave races_done unique_name top10 votes')
    .sort( 'rank_pos' )
    .lean()
    .then( async drivers => {
      let processed = drivers.map(async (e) => {
      await User.findOne({steam_id: e.steamID}).select({'_id': 1})
          .then(user =>  { e._id = user._id} )
          .catch(err => {console.log(err); throw err})
        return {
          id: e._id,
          Points: Math.round(e.points * 1000),
          Incidents: (e.incident_ave || 0.00).toFixed(2),
          Name: e.unique_name,
          First: e.top10[1],
          Second: e.top10[2],
          Third: e.top10[3],
          Fourth: e.top10[4],
          Fifth: e.top10[5],
          Races: e.races_done,
          Class: e.classimg,
          Position: e.rank_pos,
          votes: e.votes,
        }
      })
      let response =  await Promise.all(processed)
      res.status(200).json({ driversStats: response})
    })

    .catch( err => {
      console.log(err)
      res.status(404).json( {error: true, message: err.message} )
    })
}


const IncidentsAverage = ( incidents, races ) => {
  let incidentsAvg = races === 0 ? "0.00" :  (incidents / races).toFixed(2)
  return incidentsAvg
}
