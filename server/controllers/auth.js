import passport from 'passport'
import jwt from 'jsonwebtoken'
import SteamStrategy from 'passport-steam'
import config from 'config'
import User from '../models/user.model'
import Driver from '../models/driver.model'

const hostName = config.get('Valley.hostName')
const returnAuthentification = config.get('Valley.returnAuthentification')
const steamApiKey = config.get('Valley.steamApiKey')
const secretWord = config.get('Valley.secretWord')

passport.use(new SteamStrategy({
  returnURL: hostName + '/api/auth/steam/return',
  realm: hostName + '/api',
  apiKey: steamApiKey,
  },
  function(identifier, profile, done){
    User.findOne({ steam_id: profile._json.steamid }).lean()
      .then(user => {
        if(!user){
          User.countDocuments({}).then((response) => {
            const nextId = ('00000' + (response + 1)).slice(-5)
            let newUser = new User({
              _id: nextId,
              steam_id: profile._json.steamid,
              username: profile._json.personaname,
              avatar: profile._json.avatarfull,
            })
            newUser.save()
              .then(user => {
                let newDriver = new Driver({steamID: user.steam_id})
                Driver.countDocuments({}).then((totalRank) => {
                  newDriver.rank_pos= totalRank + 1
                  newDriver.save()
                })
                .then(driver => done(null, newUser))
              })
              .catch((err) => {
              if(err) return done(err, newUser)
              done(null, newUser)
            })
          })
          .catch(err => { 
            console.log(err)
            done( err, null )
          })
        }
        else{
          return done(null, user)
        }
       }).catch(err => {
         console.log(err)
         done(err, null)
       })
  }
))


exports.steamRedirect = passport.authenticate('steam')


exports.steamMiddleware =  passport.authenticate('steam',
    {
      session: false,
      failureRedirect: '/'
    }
  )
exports.steamSuccess = (req, res) => {
    let token = jwt.sign({userId: req.user._id, username: req.user.username}, secretWord)
    res.cookie('userId', req.user._id.toString())
    res.cookie('token', token)
    res.cookie('username', req.user.username)
    res.cookie('email_confirmed', req.user.email_confirmed)
    res.cookie('sponsor', req.user.sponsor)
    req.user.email_confirmed ?
      res.redirect(returnAuthentification + '/drivers/' + req.user._id)
    : res.redirect(returnAuthentification + '/users/' + req.user._id)
}

exports.protected = ( req, res, next ) => {
  try{
    let token = req.headers['x-access-token']
    let decoded = jwt.verify(token, secretWord)
    User.findOne({_id: decoded.userId})
      .then(user => {
        if(user)
          {
            req.userId = decoded.userId
            req.username = decoded.username
            next()
          }
        else res.status(404).json({error: true, message: 'No user found'})
    })
    .catch(err => {
      res.status(500).json({error: true, message: 'Problem to find the user'})
    })
    }
  catch(err){
    res.status(401).json({error: true, message: 'User not authenticated'})
  }
}

