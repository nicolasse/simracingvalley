import User from '../models/user.model'
import nodemailer from 'nodemailer'
import confirmateEmail from '../helpers/emailConfirmationFormat'
import jwt from 'jsonwebtoken'
import config from 'config'

const email = config.get('Valley.email')
const secretWord = config.get('Valley.secretWord')

exports.list = (req, res) => {
  User.find().select({'password': 0}).lean()
    .then(result => res.status(200).json(result))
    .catch(err => console.log(err))
}

exports.detail = ( req, res ) => {
  User.findOne({_id: req.params.id})
    .select('username email lastname name about birthday city gender phrase state avatar')
    .then(user => {
      res.status(200).json(user)
    })
    .catch(err => {
      res.status(500).json({error: true, message: 'User not find'})
    })
}

exports.save = (req, res) => {
  User.findOne({_id: req.params.id})
    .then( user => {
      let newUser = req.body
      let newEmail = newUser.email
      newUser.email = ''
      for(let key in newUser){
        let newData = newUser[key]
        if (newData !== ''){
          user[key] = newData
        }
      }
      if(user.email !== newEmail){
      user.email = newEmail
      user.email_confirmed = false
      let token = jwt.sign({id: user._id, email: req.body.email}, secretWord)
      user.email_token = token

      let mailConfig = {...email}
      let mailOptions = {from: 'SIMRACING VALLEY <'+email.auth.user+'>', to: user.email, subject: 'Confirmação de Email', html:confirmateEmail.confirmationFormat(token)}
      let trasporter = nodemailer.createTransport(email)
      trasporter.sendMail(mailOptions).then(info => user.save())
       }
      else{return user.save()}
    })

    .then(response => res.status(200).json({saved: true}))
    .catch(err => {
      console.log(err);
      res.status(500).json({error: true, message: 'Update DB error'})})
}
