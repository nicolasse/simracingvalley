import express from 'express'
import passport from 'passport'
import SteamStrategy from 'passport-steam'
import auth from '../controllers/auth'
import RaceController from '../controllers/race.controller'
import DriverController from '../controllers/driver.controller'
import UserController from '../controllers/user.controller'
import EmailVerificationController from '../controllers/emailVerification.controller' 
import RecordController from '../controllers/record.controller'
import VoteController from '../controllers/vote.controller'
import ScheduleController from '../controllers/schedule.controller'


const router = express.Router()

router.get('/drivers/page/:page', DriverController.list)
router.get('/drivers/:id', DriverController.detail)
router.get('/drivers/search/:name', DriverController.search)

router.get('/records', RecordController.listTracks )
router.get('/records/:track', RecordController.listCars)
router.get('/records/:track/:car', RecordController.listRecords)

router.get('/races/page/:page', RaceController.list)
router.get('/races/:id', RaceController.detail)
router.post('/races/:id/vote/:driver',auth.protected, VoteController.vote)


router.get('/users', UserController.list)
router.get('/users/:id', UserController.detail)
router.put('/users/:id', auth.protected, UserController.save)

router.get('/auth/steam', auth.steamRedirect)
router.get('/auth/steam/return', auth.steamMiddleware, auth.steamSuccess)

router.get('/schedule', ScheduleController.list)

router.get('/confirm/:token', EmailVerificationController.confirmation)
module.exports = router
